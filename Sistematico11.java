package Trabajo;
import java.util.Scanner;

public class Sistematico11 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = cantAct(scan);
        ActivoFijo activo[]=new ActivoFijo[n];

        boolean salir = false;
        int op;
        int pos=0;
        do{
            menu();
            op=scan.nextInt();
            switch(op){
                case 1:
                    if(pos!=n){
                        System.out.println("Colocar nuevo activo fijo...");
                        activo[pos]=createActivo(scan);
                        pos++;
                    }else{
                        System.out.println("Cantidad maxima de activos alcanzada.");
                    }                    
                    break;

                case 2:
                    System.out.println("Visualizando...");
                    System.out.println("Años");
                    for (int i=0;i<=activo.length;i++){
                        System.out.print(activo);
                    }
                    
                    break;

                case 3:
                    int ocp;
                    boolean sal=false;
                    do{                        
                        dep();
                        ocp=scan.nextInt();
                        switch(ocp){
                            case 1:
                                break;
                            case 2:
                                break;
                            case 3:
                                sal=true;
                                System.out.println("Gracias por usar el programa.");
                                break;
                            default:
                                System.out.println("Opcion Invalida, intente nuevamente.");
                                break;
                        }

                    }while(!sal);
                    break;

                case 4:
                    salir=true;
                    System.out.println("Gracias por usar el programa.");
                    break;
                default:
                    System.out.println("Opcion Invalida, intente nuevamente.");
                    break;
                }
        }while(!salir);
    }

    public static void menu(){ //imprimir menu
        
        System.out.println("\n\n\n----Menu----");
        System.out.println("1. Agregar nuevo activo fijo.");
        System.out.println("2. Visualizar todos los activos fijos.");
        System.out.println("3. Calcular depreciación de todos los activos fijos.");
        System.out.println("4. Salir.");
        System.out.print("Seleccione una opcion:");
        
    }
    public static void dep(){
        System.out.println("\n\n\n-Seleccionar un metedo de depreciacion desea utilizar-");
        System.out.println("1. Linea Recta.");
        System.out.println("2. Sistema de digitos de los años incremental.");
        System.out.println("3. Salir.");
        System.out.print("Seleccione una opcion:");
        
    }
    public static int cantAct(Scanner reader){
        int cant;
        do{
            System.out.print("Ingrese el numero de activos a agregar: ");
            cant=reader.nextInt();
        }while(cant<1);
        return cant;
    }
    public static ActivoFijo createActivo(Scanner reader){
        if(reader == null){
            return null;
        }
        System.out.println("\n\nIngrese el codigo: ");
        String codigo = reader.next();
        System.out.println("Ingrese el nombre: ");
        String nombre = reader.next();
        System.out.println("Ingrese la descripcion: ");
        String descripcion = reader.next();
        System.out.println("Ingrese el tipo de activo: ");
        String tipo = reader.next();
        System.out.println("Ingrese la cantidad: ");
        int cantidad = reader.nextInt();
        System.out.println("Ingrese el valor: ");
        int valor = reader.nextInt();
        System.out.println("Ingrese la fecha de compra: ");
        String fecha = reader.next();
        

        return createActivo(reader);
    }
}

